.. include:: ../README.rst

API Documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:

   odf

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   _tmp/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
